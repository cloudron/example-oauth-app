#!/usr/bin/env node

/* jshint esversion:6 */

const express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    CloudronStrategy = require('passport-cloudron'),
    fs = require('fs'),
    passport = require('passport'),
    path = require('path'),
    util = require('util'),
    session = require('express-session');

function exit(error) {
    if (error) console.error(error);
    process.exit(error ? 1 : 0);
}

function ensureEnv(env) {
    if (!process.env[env]) exit('No ' + env + ' configured. Cannot continue.');
}

ensureEnv('OAUTH_CLIENT_ID');
ensureEnv('OAUTH_CLIENT_SECRET');
ensureEnv('API_ORIGIN');
ensureEnv('APP_ORIGIN');

var port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;
var root = process.argv[2] || path.resolve('public');

function AuthError(error, status) {
    Error.call(this);
    Error.captureStackTrace(this, this.constructor);

    this.message = error;
    this.status = status;
}
util.inherits(AuthError, Error);

var strategy = new CloudronStrategy({
    callbackURL: process.env.APP_ORIGIN + '/_cloudron/callback'
}, function verify(token, tokenSecret, profile, done) {
    done(null, profile);
});

strategy.parseErrorResponse = function (bodyString, status) {
    return new AuthError(bodyString, status);
};

passport.serializeUser(function (user, done) {
    done(null, JSON.stringify(user));
});

passport.deserializeUser(function (str, done) {
    try {
        done(null, JSON.parse(str));
    } catch (e) {
        done(e);
    }
});

passport.use(strategy);

var app = express();

app.set('trust proxy', 1);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ cookie: { path: '/' }, saveUninitialized: true, resave: true, secret: 'the squirrel crossed the road' }));
app.use(passport.initialize());
app.use(passport.session());

app.get('/_cloudron/healthcheck', function (req, res) { res.status(200).end(); });
app.get('/_cloudron/login', function (req, res) { res.sendFile(path.join(root, 'login.html')); });
app.get('/_cloudron/oauth', passport.authenticate('cloudron', { failureRedirect: '/_cloudron/login' }), function (req, res) { res.redirect('/'); });
app.get('/_cloudron/callback', passport.authenticate('cloudron', { failureRedirect: '/_cloudron/login' }), function (req, res) { res.redirect('/'); });

// make all requests go through auth
app.use(function (req, res, next) {
    if (!req.isAuthenticated()) return res.redirect('/_cloudron/login');
    next();
});

app.get('/_cloudron/logout', function (req, res) {
    req.logout();
    res.redirect(process.env.API_ORIGIN + '/api/v1/session/logout?redirect=' + process.env.APP_ORIGIN);
});

// hook for main app
app.use(express.static(root));

// basic error handling
app.use(function (err, req, res, next) {
    if (err instanceof AuthError) {
        console.log('AuthError:', err);
        return res.redirect('/_cloudron/login');
    }

    next();
});

app.listen(port, function () {
    console.log('Listening on port %s', port);
    console.log('Serving up directory %s', root);
});
