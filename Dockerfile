FROM cloudron/base:0.10.0
MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

ENV PATH /usr/local/node-6.9.5/bin:$PATH

COPY public/ /app/code/public/
ADD start.sh package.json index.js /app/code/

RUN npm install

CMD [ "/app/code/start.sh" ]
